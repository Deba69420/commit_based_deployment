trigger SendConfirmationEmail on Session_Speaker1__c (after insert) {
set<Id> l=new set<Id>();
   for(Session_Speaker1__c newItem:trigger.new)
    {
        l.add(newItem.Id);
    }
  List<Session_Speaker1__c> sessionspeaker=[SELECT Session__r.Name,
                                            Session__r.SessionDate__c,
                                            Speaker__r.First_Name__c,
                                            Speaker__r.Last_Name__c,
                                            Speaker__r.Email__c from Session_Speaker1__c  WHERE Id IN:l
                                           AND Speaker__r.Email__c<>NULL];
    
    if(sessionspeaker.size()>0)
    {
        string[] emailid=new string[]{},
            subject= new string[]{},
                message =new string[]{};
                    for(Session_Speaker1__c sessionspeaker:sessionspeaker)
                {
                    emailid.add(sessionspeaker.Speaker__r.Email__c);
                    subject.add('speaker confirmation');
                    message.add('Dear'+sessionspeaker.Speaker__r.First_Name__c+
                               ',\nyour session "'+sessionspeaker.Session__r.Name+'"on'+
                                sessionspeaker.Session__r.SessionDate__c+'is confirm.\n\n'+
                               'thanks for speaking in confrence');
                }
        EmailManager.sendMail(emailid,subject,message);
    }
}