<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <brand>
        <headerColor>#0070D2</headerColor>
        <shouldOverrideOrgTheme>false</shouldOverrideOrgTheme>
    </brand>
    <description>Suggestion Box mobile App</description>
    <formFactors>Large</formFactors>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <label>Suggestion Box</label>
    <navType>Standard</navType>
    <profileActionOverrides>
        <actionName>Tab</actionName>
        <content>HomePage11</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>standard-home</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>Standard</profile>
    </profileActionOverrides>
    <tabs>Suggestion__c</tabs>
    <tabs>standard-report</tabs>
    <tabs>standard-Dashboard</tabs>
    <tabs>Airtel__c</tabs>
    <tabs>IDEA__c</tabs>
    <tabs>Expense__c</tabs>
    <tabs>Tower__c</tabs>
    <tabs>Energy_Audit__c</tabs>
    <uiType>Lightning</uiType>
    <utilityBar>Suggestion_Box_UtilityBar</utilityBar>
</CustomApplication>
